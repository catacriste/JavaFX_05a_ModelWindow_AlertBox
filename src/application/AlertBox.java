package application;



import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/*Esta classe cria uma janela modal no m�todo display , para servir de AlertBox
 * Recebe 2 parametros : t�tulo para a janela e texto para a mensagem. Nada Devolve
 * Uma janela modal � uma janela filha criada a partir de outra (pai).
 * Modal prende o controlo, impedindo o acesso � janela Mae e obrigando  o user a fazer algo na filha
 * 
 * Nota Final: Os m�todos static n�o s�o instanci�veis, s�o usados diretamente atrav�s da classe*/

public class AlertBox {
	public static void display(String title, String msg){ //Static para n�o ser instanciada
		
		Stage janela = new Stage();							//Cria uma window
		//janela.initModality(Modality.APPLICATION_MODAL);	//Define uma janela Modal
		janela.initModality(Modality.WINDOW_MODAL);	//Define uma janela Modal
		janela.setTitle(title); 							//Como t�tulo, recebe a string do parametro
		janela.setMinWidth(200);							//Largura da janela
		
		Label mensagem = new Label(msg); 					//Cria a label para mostra
		Button btnClose = new Button("Fechar");				//Cria bot�o para fechar janela
		btnClose.setOnAction(e -> janela.close());			//A��o fecha esta janela
		
		VBox layout = new VBox(10);							//Layout vertical com 10px entre c�lulas
		layout.getChildren().addAll(mensagem, btnClose);	//Adiciona Label e Button ao layout
		layout.setAlignment(Pos.CENTER);					//Alinhar os cnteudos ao Centros
		
		Scene scene = new Scene(layout);					//Criar a Scene e associa o Layout
		janela.setScene(scene);								//Associa a Scena 
		janela.showAndWait();								//Executa e prende o controlo at� ser fechada
		
		
	}

}
