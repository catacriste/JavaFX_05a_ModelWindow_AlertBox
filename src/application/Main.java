package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

/*Modal Window
 * - � uma janela filha, que abre dentro da janela principal
 * - Neste exemplo vamos criar uma alertBox ativada apartir da janela principal, por bot�o
 * - passo 1 - Criar uma classe java : rato dr� em src- new java class : AlertBox
 * - passo 2 - chamar a nova janela apartir da a��o de um bot�o . O facto de ser uma modal
 * 				, implica que o ocntrolo fique nela at� ser fechada, ou seja, o user nao 
 * 				consegue aceder � principal, enquanto a modal estiver aberta */


public class Main extends Application {
	Button btnAlert;
//Cria Bot�o null, fora dos m�todos para ser acess�vel a todos
	@Override
	public void start(Stage primaryStage) {
		try {
		
			btnAlert = new Button("Clica-me");						//Inicializa��o
			btnAlert.setOnAction(e->AlertBox.display("Cuidado", "isto � uma AlertBox"));
			
			StackPane layoutRoot = new StackPane(); //layout Principal
			layoutRoot.getChildren().add(btnAlert);
			
			Scene scene = new Scene(layoutRoot, 400,600);
			
			primaryStage.setScene(scene);
			primaryStage.setTitle("Janela Modal");
			primaryStage.show();
			
			
			
			
			
			
			
		} catch(Exception e) {					//Tratamento Gen�rico das Exe��es
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		launch(args);
	}
}